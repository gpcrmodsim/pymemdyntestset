#!/bin/bash
#set -x
#trap read debug

export rcran=/home/apps/R
export PATH=$rcran/bin:$PATH

echo "The following are the wall-clock times in hours: "
for i in {1..10}
do
    cd ../test$i/finalOutput/logs
    echo test$i
    grep "Time:" *.log | awk '{print $4}' | Rscript -e "A <- read.table(pipe('cat /dev/stdin')); (sum(A)/60)/60"
    echo " "
    cd ../../
done
    
