#!/bin/bash
#set -x
#trap read debug

for i in {1..10}
do
    cd ../test$i/finalOutput

    cp ../../analysis/rmsd-calpha.agr .
    sed '1,13d' rmsd-calpha-vs-start.xvg | awk '{print $2}' > t1
    sed '1,13d' rmsd-calpha-vs-start.xvg | awk '{print $1, $2}' > t2
    paste t2 t1 > rmsd-calpha-vs-start.dat
    j=`sed ''''$i'''q;d' ../../analysis/uniprot.dat`
    sed -i 's/aa1r_human/'''$j'''/g' rmsd-calpha.agr
    xmgrace -batch rmsd-calpha.agr -nosafe -hardcopy

    cp ../../analysis/rmsf-per-residue.agr .
    sed '1,12d' rmsf-per-residue.xvg | awk '{print $2}' > t1
    sed '1,12d' rmsf-per-residue.xvg | awk '{print $1, $2}' > t2
    paste t2 t1 > rmsf-per-residue.dat
    sed -i 's/aa1r_human/'''$j'''/g' rmsf-per-residue.agr
    xmgrace -batch rmsf-per-residue.agr -nosafe -hardcopy
    rm t1 t2

    echo "#!/usr/bin/env python
load hexagon.pdb
load_traj traj_pymol.xtc, hexagon
hide lines, hexagon
select calpha, name CA
set ribbon_trace_atoms, 1
show ribbon, calpha
spectrum count, rainbow, calpha
#spectrum count, blue_red, calpha, minimum=5, maximum=200
intra_fit calpha
set all_states, 1
hide everything, not calpha
rotate x, -90
zoom calpha, 1
set ray_opaque_background, 0
turn y, -30
png calphasuper.png, width=1200, height=1200, dpi=300, ray=1
deselect
" > bwtraj.pml
    pymol -qc bwtraj.pml
    convert -composite rmsd_calpha.png calphasuper.png -geometry +80+300 super.png
    cd ../
    echo $PWD
done
    
cd ../analysis

list=`for j in {1..10}; do echo "../test'''$j'''/finalOutput/super.png"; done | xargs`
montage `echo $list` -mode concatenate -tile 2x5 rmsd_mosaic.png

unset list
list=`for j in {1..10}; do echo "../test'''$j'''/finalOutput/rmsf-per-residue.png"; done | xargs`
montage `echo $list` -mode concatenate -tile 2x5 rmsf_mosaic.png



