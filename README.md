% Eleven test cases for **Ballesteros-Weinstein** based NMR-style restraints using pymemdyn.
% Mauricio Esguerra Neira
% \date{}
## 29/07/2016

We are using 24 pair-distance restraints between carbon alpha atoms as
shown in the next table.
The source files needed for these tests is at a github repo at:
[https://esguerra@bitbucket.org/gpcrmodsim/pymemdyntestset.git](https://esguerra@bitbucket.org/gpcrmodsim/pymemdyntestset.git) 


| pairnum | res1 | res2 |
|:-------:|:----:|:----:|
| 1       | 1.46 | 7.47 |
| 2       | 1.49 | 7.50 |
| 3       | 1.50 | 2.47 |
| 4       | 1.50 | 2.50 |
| 5       | 1.50 | 7.46 |
| 6       | 1.53 | 2.47 |
| 7       | 1.57 | 2.44 |
| 8       | 2.42 | 3.46 |
| 9       | 2.43 | 7.53 |
| 10      | 2.50 | 7.46 |
| 11      | 3.34 | 4.53 |
| 12      | 3.34 | 4.57 |
| 13      | 3.36 | 6.48 |
| 14      | 3.38 | 4.50 |
| 15      | 3.38 | 4.53 |
| 16      | 3.40 | 6.44 |
| 17      | 3.44 | 5.54 |
| 18      | 3.47 | 5.57 |
| 19      | 3.51 | 5.57 |
| 20      | 3.51 | 5.60 |
| 21      | 5.54 | 6.41 |
| 22      | 6.47 | 7.45 |
| 23      | 6.51 | 7.38 |
| 24      | 6.51 | 7.39 |

These 24 restraints are applied to the set of 11 GPCR's described in
the following table.


| testname | Receptor                        | Ligand  |   Uniprot   |    Notes   | Crystal |
|:--------:|:-------------------------------:|:-------:|:-----------:|:----------:|:-------:|
| test1    | Angiotensin II type I           | peptide | agtr1_human | two chains |  4yay   |
| test2    | Adenosine a1                    | none    |  aa1r_human | none       |    no   |
| test3    | Adenosine a2a                   | none    | aa2ar_human | none       |  4eiy   |
| test4    | Adenosine a2b                   | none    | aa2br_human | none       |    no   |
| test5    | Adenosine a3                    | none    |  aa3r_human | none       |    no   |
| test6    | Serotonin (5-hydroxytryptamine) | none    | 5ht2b_human | none       |  4ib4   |
| test7    | Neuropeptide Y type I           | none    | npy1r_human | none       |    no   |
| test8    | a2a_ag standard test with all   | uk432097| aa2ar_human | water, Na  |  4eiy   |
| test9    | C-X-C chemokine type IV         | none    | cxcr4_human | none       |  3odu   |
| test10   | D3 dopamine                     | none    |  drd3_human | none       |  3pbl   |
| test11   | random sequence                 | none    |  none       | none       |  none   |
| ~~test12~~ | ~~Adenosine dimer~~           | ~~none~~|  ~~none~~   | ~~none~~   |~~none~~ |


In the cases where there are  crystal structures available we will use
them to compute  the RMSD but also to use  them as starting structures
to  see  how  they  are  deformed by  the  restrained  dynamics.   All
structures were aligned to a2a_ag  since there is no alignment routine
included in pymemdyn yet.  


## test1
As starting structures we're using the inactive structure of Jessica's
multi-template model.  

There  is a  crystal structure  to  compare to.  
This has  PDB_IB=4yay corresponding to the article:  

http://dx.doi.org/10.1016/j.cell.2015.04.011

The crystal structure has BRIL in the extracellular side linked to the
N-terminal side. Intracellular loop 3  and parts of extracellular loop
2 are missing.  

Old tests which used the  sequence of AT1R_human available at uniprot,
which in  turn has  longer n-ter  and c-ter are  moved to  the oldtest
folder.  


## test2
Adenosine receptor aa1r_human structure coming from Ana's simulations.


## test3
Adenosine receptor aa2ar_human structure coming from Ana's simulations.


## test4
Adenosine receptor aa2br_human structure coming from Ana's simulations.


## test5
Adenosine receptor aa3r_human structure coming from Ana's simulations.


## test6

The sequence for 5ht2b_human is  used at the gpcr-modsim.org webserver
for homology  modeling using the  14_inactive templates, that  is, the
templates available before the 4ib4.pdb  structure of the receptor was
made available.
The c-ter is shortened.
Residues after 281 are not modeled.
gpcr-modsim.org suggest to use as template 2vt4.pdb.
We generate 50 models and retain  the top ranking one to continue with
the BW restrained modeling.


## test7
Here we  are using a modeled  structure in the inactive  state made by
Hugo.


## test8
This is a standard  test coming from Hugo with all  toys in, that is,
orthosteric ligand, alosteric, cholesterol  molecules, sodium ions and
crystal waters. The orthosteric ligand seems to be UK432097 which is
found in the 3QAK pdb structure of an active human a2a adenosine
receptor, so, most likely this is an agonist.


## test9
Took the sequence from uniprot and feed it to gpcr-modsim.
Model 50 homology models.
Take the highest ranked one.
Shortened the N-term. Deleted first 31 residues.
deleted C-term residues 326 to 352.


## test10
Took the sequence from uniprot and feed it to gpcr-modsim.
Model 50 homology models.
Take the highest ranked one.
Shortened the N-term. Deleted first 30  residues, but left a very long
ICL3.
On a second test of 10ns total dynamics I have taken the ICL3 out by
taking the sequence of the best model and aligning it with clustalw to
the sequence of the crystal and getting rid of the non-matching areas
and the ICL3 loop region.


## test11
Made a random aminoacid sequence and sent it to gpcr-modsim.
Used the highest ranked homology model and continued with dynamics.


## test12

This  will be  the  toughest test.  Just  an idea  for  now.  Two  fat
problems  can  be foreseen,  how  to  make  sure that  the  assignment
translates well to another chain, the dimer one, and also if there are
distance restraints to be imposed inter-monomer.


# Questions for Hugo

+ Use ligands? A. **Yes**  
If so which and in which cases.  See test 8.  

+ In test 8 I'm using a ligand, which ligand is it?  
A. It seems to be the agonist UK432097 from PDB-ID=3QAK  

+ How long to run? For now 5ns total with 2.5ns in the B.W. restraints part.
Maybe do 5ns and 10ns for the B.W. restrained part? A. **Yes**  


# To do

[*] Once the implementation is ready compare doing 10 ns c-alpha and 10 ns
with BW restraints for the ten tests.   

[*] Add command at the end of produced.xvg files to produce pdf's from
command line.  

[*] Make random sequence example.  


