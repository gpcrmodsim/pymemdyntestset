#!/usr/bin/python
import random
import sys

# Print the script usage info if the user
# doesn't supply the correct number of arguments.
if len(sys.argv) != 2: 
        print 'Usage: randomaa.py <num_residues>'
        sys.exit(1)

# Read from user the length of the random protein sequence
# that you want to generate.
arg1 = sys.argv[1]

# Create output file.
# Generate random sequence.
# Write sequence to output.
outseq = open('aaseq%s.seq' % arg1, 'w')
peptide = ''.join([random.choice('ABCDEFGHIJKLMNOPQRSTUVWYZ') for x in range(int (arg1))])
outseq.write(peptide)

    
    
