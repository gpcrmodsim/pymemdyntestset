#!/bin/bash
cp at_B99990027.pdb       test1/at_B99990027.pdb 
cp aa1r_human.pdb         test2/aa1r_human.pdb 
cp aa2ar_human.pdb        test3/aa2ar_human.pdb 
cp aa2br_human.pdb        test4/aa2br_human.pdb 
cp aa3r_human.pdb         test5/aa3r_human.pdb 
cp models_-34218_2vt4.pdb test6/models_-34218_2vt4.pdb 
cp larsmodel.pdb          test7/larsmodel.pdb 
cp models_-36333_3odu.pdb test9/models_-36333_3odu.pdb
cp models_-39256_3pbl.pdb test10/models_-39256_3pbl.pdb

rm *.pdb
