#!/bin/env python
set retain_order, 1
load  test1/at_B99990027.pdb,       t1
load  test2/aa1r_human.pdb,         t2
load  test3/aa2ar_human.pdb,        t3
load  test4/aa2br_human.pdb,        t4
load  test5/aa3r_human.pdb,         t5
load  test6/models_-34218_2vt4.pdb, t6  
load  test7/larsmodel.pdb,          t7
load  test8/a2a_ag.pdb,             t8
load  test9/models_-36333_3odu.pdb, t9
load test10/models_-39256_3pbl.pdb, t10

cealign t1,  t8       
cealign t2,  t8         
cealign t3,  t8        
cealign t4,  t8        
cealign t5,  t8         
cealign t6,  t8 
cealign t7,  t8          
cealign t9,  t8 
cealign t10, t8

save  at_B99990027.pdb, t1
save  aa1r_human.pdb, t2
save  aa2ar_human.pdb, t3
save  aa2br_human.pdb, t4
save  aa3r_human.pdb, t5
save  models_-34218_2vt4.pdb, t6
save  larsmodel.pdb, t7
#save  a2a_ag.pdb, t8
save  models_-36333_3odu.pdb, t9
save  models_-39256_3pbl.pdb, t10

