#!/bin/bash
echo "#!/bin/bash -l
module load gromacs/4.6.3
module load python/2.7.6
a2a_ag.pdb -l lig -w hoh -c cho -i ions_local
" > temp.sh
chmod +x temp.sh
sbatch -A snic2015-1-304 -n 32 -t 30:00:00 -J pymemdyn temp.sh
